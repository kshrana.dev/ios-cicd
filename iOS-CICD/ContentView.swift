//
//  ContentView.swift
//  iOS-CICD
//
//  Created by Kishor Rana on 27.03.20.
//  Copyright © 2020 Kishor Rana Engineering. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
